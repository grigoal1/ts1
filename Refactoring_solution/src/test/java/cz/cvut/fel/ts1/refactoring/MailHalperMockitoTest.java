package cz.cvut.fel.ts1.refactoring;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.mail.Message;
import javax.mail.Transport;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;


@RunWith(PowerMockRunner.class)
@PrepareForTest(Transport.class)
public class MailHalperMockitoTest {

    @BeforeEach
    public void setUp() throws Exception{
        PowerMockito.mockStatic(Transport.class);
        PowerMockito.doNothing().when(Transport.class, "send", any(Message.class));
    }

    @Test
    public void createAndSendMail_ValidEmailParametersPassed_SaveMailMethodCalledTwice(){
        //arrage

        IDBManager mockedDbManager = mock(IDBManager.class);
        String to = "to@fel.cvut.cz";
        String subject = "Mail subject";
        String body = "body";
        Mail mailToReturn = new Mail();
        mailToReturn.setTo(to);
        mailToReturn.setSubject(subject);
        mailToReturn.setBody(body);
        mailToReturn.setIsSent(false);
        when(mockedDbManager.findMail(anyInt())).thenReturn(mailToReturn);
        MailHelper helper = new MailHelper(mockedDbManager);

        //act
        helper.createAndSendMail(to, subject, body);
        try{
            helper.sendingThread.join();
        }catch (InterruptedException ex){
            fail(ex.getMessage());
        }


        //assert
        verify(mockedDbManager,times(2)).saveMail(any(Mail.class));
    }
}
