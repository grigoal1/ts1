package cz.cvut.fel.ts1.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FlightOrderPage {
    private WebDriver driver;
    private By firstName = By.id("firstName");
    private By lastName = By.id("lastName");
    private By email = By.id("email");
    private By birthDate= By.id("birthDate");
    private By destination = By.id("destination");
    private By submit = By.cssSelector("body > form > div > button");

//    /html/body/form/div/button – XPath
//    body > form > div > button - Selector
//    private By discount = By.id("discount");
    public static FlightOrderPage goTo(String baseUrl, WebDriver driver){
        driver.get(baseUrl);
        FlightOrderPage flightOrderPage = new FlightOrderPage(driver);
        Wait<WebDriver> wait = new WebDriverWait(driver, 10);
        wait.until((ExpectedCondition<Boolean>) driver1 -> driver1.findElement(By.id("firstName")).isDisplayed());
        return flightOrderPage;

    }

    public FlightOrderPage(WebDriver webDriver){
        this.driver = webDriver;
    }

    public void setFirstName(String strFirstName){
        driver.findElement(firstName).sendKeys(strFirstName);
    }

    public void setLastName(String strFirstName){
        driver.findElement(lastName).sendKeys(strFirstName);
    }

    public void setEmail(String strFirstName){
        driver.findElement(email).sendKeys(strFirstName);
    }

    public void setBirthDate(int year, int mouth, int day){
        Calendar cal = Calendar.getInstance();
        cal.set(year, mouth, day);
        Date date = cal.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String formattedBirthDate = null;
        try {
            formattedBirthDate = dateFormat.format(date);
        }catch (Exception e){
            e.printStackTrace();//todo...
        }
        driver.findElement(birthDate).sendKeys(formattedBirthDate);
    }

    public void setDestination(String destinationVisibleText){
        Select selectDestination = new Select(driver.findElement(destination));
        selectDestination.selectByVisibleText(destinationVisibleText);
    }

    public void clickCheckBox(String checkboxIdToCheck){
        driver.findElement(By.id(checkboxIdToCheck)).click();
    }

    public void submitOrder(){
        driver.findElement(submit).click();
    }

}
