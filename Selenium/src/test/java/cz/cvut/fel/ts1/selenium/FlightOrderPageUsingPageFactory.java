package cz.cvut.fel.ts1.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.NoSuchElementException;

public class FlightOrderPageUsingPageFactory {
    private WebDriver driver;

    @FindBy(id = "firstName")
    private WebElement firstName;

    @FindBy(id = "lastName")
    private WebElement lastName;

    @FindBy(id = "email")
    private WebElement email;

    @FindBy(id = "birthDate")
    private WebElement birthDate;

    @FindBy(id = "destination")
    private WebElement destination;

    @FindBy(css = "body > form > div > button")
    private WebElement submitButton;

    public FlightOrderPageUsingPageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void waitFor(){
        Wait<WebDriver> wait = new WebDriverWait(driver, 10);
        wait.until((ExpectedCondition<Boolean>) driver1 -> driver1.findElement(By.id("firstName")).isDisplayed());
    }

    public static FlightOrderPageUsingPageFactory goTo(String baseUrl, WebDriver driver) {
        driver.get(baseUrl);
        FlightOrderPageUsingPageFactory flightOrderPageUsingPageFactory = new FlightOrderPageUsingPageFactory(driver);
        flightOrderPageUsingPageFactory.waitFor();
        return flightOrderPageUsingPageFactory;
    }

    public void setFirstName(String firstName) {
        this.firstName.sendKeys(firstName);
    }

    public void setLastName(String lastName) {
        this.lastName.sendKeys(lastName);
    }

    public void setEmail(String email) {
        this.email.sendKeys(email);
    }

    public void setBirthDate(int year, int mouth, int day)    {
        Calendar cal = Calendar.getInstance();
        cal.set(year, mouth, day);
        Date date = cal.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String formattedBirthDate = null;
        try {
            formattedBirthDate = dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();//todo...
        }
        this.birthDate.sendKeys(formattedBirthDate);
    }

    public void setDestination(String destination) {
        this.destination.sendKeys(destination);
    }

    public void setSubmitButton(String submitButton) {
        this.submitButton.sendKeys(submitButton);
    }
}
