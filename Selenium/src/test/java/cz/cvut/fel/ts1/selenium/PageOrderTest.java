package cz.cvut.fel.ts1.selenium;

import cz.cvut.fel.ts1.selenium.pageobjects.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class PageOrderTest {
    ChromeDriver driver;

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("chrome.switches", "--disable-extensions");
        driver = new ChromeDriver(options);
    }

    @Test
    public void test01(){
        FlightOrderPage flightOrderPage = FlightOrderPage.goTo("http://35.198.153.63", driver);
        flightOrderPage.setFirstName("Jan");
        flightOrderPage.setLastName("Vonacka");
        flightOrderPage.setEmail("jan@vonacka.cz");
        flightOrderPage.setBirthDate(1999,1,20);
        flightOrderPage.clickCheckBox("student");
        flightOrderPage.setDestination("London");
        flightOrderPage.submitOrder();
        //todo assert
    }

    @Test
    public void test02(){
        LoginPage loginPage = LoginPage.goTo("https://link.springer.com/signup-login", driver);
        loginPage.setEmail("abc@dev.com");
        loginPage.setPassword("qwerty");
        loginPage.onClickSubmit();
        //todo assert
    }

    @Test
    public void test03(){
        MainPage mainPage = MainPage.goTo("https://link.springer.com/", driver);
        LoginPage loginPage = mainPage.onClickLogin();
        loginPage.setEmail("abc@dev.com");
        loginPage.setPassword("qwerty");
        loginPage.onClickSubmit();
        //todo assert
    }

    @Test
    public void test04(){
        MainPage mainPage = MainPage.goTo("https://link.springer.com/", driver);
        SearchPage searchPage = mainPage.onClickSearch();
        searchPage.clickResultsOnlyAccessCheckboxAdvanced();
        searchPage.clickResultsOnlyAccessCheckboxAdvanced();
//        searchPage.tmpWait();
//        searchPage.setPassword("qwerty");
//        searchPage.onClickSubmit();
        //todo assert
    }

    @Test
    public void test05(){
        MainPage mainPage = MainPage.goTo("https://link.springer.com/", driver);
        SearchPage searchPage = mainPage.onClickSearch();
        searchPage.setWords("Selenium");
        SearchResult searchResult = searchPage.clickSubmitAdvancedSearch();
//        searchResult.tmp();
        searchResult.nextPage();
        searchResult.waitFor();
//        searchResult.tmp();
        //todo assert
    }

    @Test
    public void test06(){
        MainPage mainPage = MainPage.goTo("https://link.springer.com/", driver);
        SearchPage searchPage = mainPage.onClickSearch();
        searchPage.waitFor();
        searchPage.setWords("Selenium");
        searchPage.waitFor();
        SearchResult searchResult = searchPage.clickSubmitAdvancedSearch();
        searchResult.waitFor();
        searchResult.clickContentTypeArticle();
        searchResult = SearchResult.goTo(driver.getCurrentUrl(), driver);
        String prevUrl;
        for (int i = 0; i < 4; i++) {
            prevUrl = driver.getCurrentUrl();
            searchResult.clickElement(i);
            ArticlePage articlePage = ArticlePage.goTo(driver.getCurrentUrl(), driver);
            System.out.println(articlePage.getArticleTitle());
            System.out.println(articlePage.getDoi());
            System.out.println(articlePage.getPublicTime());
            driver.get(prevUrl);
//        ArticlePage articlePage = new ArticlePage(searchResult.getResultElements().get(0).get)
        }
        //todo assert
    }

    @Test
    public void test07_HW_Part1(){
        MainPage mainPage = MainPage.goTo("https://link.springer.com/", driver);
        SearchPage searchPage = mainPage.onClickSearch();
        searchPage.waitFor();
        searchPage.setWords("Page Object Model");
        searchPage.setLeastWords("Sellenium Testing");
        searchPage.setDateFacetMode("in");
        searchPage.setFacetStartYear("2020");
//        searchPage.waitFor();
        SearchResult searchResult = searchPage.clickSubmitAdvancedSearch();
        searchResult.waitFor();
        searchResult.clickContentTypeArticle();
        searchResult = SearchResult.goTo(driver.getCurrentUrl(), driver);
        String prevUrl;
        for (int i = 0; i < 4; i++) {
            prevUrl = driver.getCurrentUrl();
            searchResult.clickElement(i);
            ArticlePage articlePage = ArticlePage.goTo(driver.getCurrentUrl(), driver);
            System.out.println(articlePage.getArticleTitle());
            System.out.println(articlePage.getDoi());
            System.out.println(articlePage.getPublicTime());
            driver.get(prevUrl);
//        ArticlePage articlePage = new ArticlePage(searchResult.getResultElements().get(0).get)
        }
        //todo assert
    }

    @After
    public void cleanUp(){
        driver.quit();
    }
}
