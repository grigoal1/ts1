package cz.cvut.fel.ts1.selenium.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ArticlePage extends Page{
    @FindBy(css = "#main-content > main > article > div.c-article-header > header > h1")
    private WebElement articleTitle;

    @FindBy(css = "#article-info-content > div > div > ul.c-bibliographic-information__list > li.c-bibliographic-information__list-item.c-bibliographic-information__list-item--doi > p > span.u-clearfix.c-bibliographic-information__value > a")
    private WebElement doi;

    @FindBy(css = "#article-info-content > div > div > ul.c-bibliographic-information__list > li:nth-child(1) > p > span.u-clearfix.c-bibliographic-information__value > time")
    private WebElement publicTime;

    public ArticlePage(WebDriver driver) {//todo
        super(driver);
    }

    public void waitFor() {
        waitForCss("#logo > picture > img");
    }

    public static ArticlePage goTo(String baseUrl, WebDriver driver) {
        driver.get(baseUrl);
        ArticlePage articlePage = new ArticlePage(driver);
        articlePage.waitFor();
        return articlePage;
    }

    public static ArticlePage goTo(WebDriver driver) {
        ArticlePage articlePage = new ArticlePage(driver);
        articlePage.waitFor();
        return articlePage;
    }

    public String getArticleTitle() {
        return articleTitle.getText();
    }

    public String getDoi() {
        return doi.getText();
    }

    public String getPublicTime() {
        return publicTime.getText();
    }

}
