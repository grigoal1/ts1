package cz.cvut.fel.ts1.selenium.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends Page{
    @FindBy(id = "login-box-email")
    private WebElement email;

    @FindBy(id = "login-box-pw")
    private WebElement password;

    @FindBy(id = "logo")
    private WebElement logo;

    @FindBy(css = "#login-box > div > div.form-submit > button")
    private WebElement submitBtn;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void waitFor(){
        waitFor("login-box-email");
    }

    public static LoginPage goTo(String baseUrl, WebDriver driver) {
        driver.get(baseUrl);
        LoginPage loginPage = new LoginPage(driver);
        loginPage.waitFor();
        return loginPage;
    }

    public static LoginPage goTo(WebDriver driver) {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.waitFor();
        return loginPage;
    }

    public void setEmail(String email) {
        this.email.sendKeys(email);
    }

    public void setPassword(String password) {
        this.password.sendKeys(password);
    }

    public void onClickSubmit(){
        submitBtn.click();
    }

    public void onClickLogo(){
        logo.click();
    }
}
