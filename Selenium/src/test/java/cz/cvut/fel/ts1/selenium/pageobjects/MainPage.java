package cz.cvut.fel.ts1.selenium.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends Page{
    @FindBy(css = "#header > div.cross-nav.cross-nav--wide > div.auth.flyout > a")
    private WebElement login;

    @FindBy(css = "#search-options > button")
    private WebElement openSearch;

    @FindBy(id = "advanced-search-link")
    private WebElement search;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public void waitFor() {
        waitFor("header");
    }

    public static MainPage goTo(String baseUrl, WebDriver driver) {
        driver.get(baseUrl);
        MainPage mainPage = new MainPage(driver);
        mainPage.waitFor();
        return mainPage;
    }

    public LoginPage onClickLogin(){
        login.click();
        return LoginPage.goTo(driver);
    }

    public SearchPage onClickSearch(){
        openSearch.click();
        search.click();
        return SearchPage.goTo(driver);
    }


}
