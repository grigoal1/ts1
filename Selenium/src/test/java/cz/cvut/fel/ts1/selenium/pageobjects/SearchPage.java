package cz.cvut.fel.ts1.selenium.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class SearchPage extends Page{
    @FindBy(id = "all-words")
    private WebElement words;

    @FindBy(id = "exact-phrase")
    private WebElement exactPhrase;

    @FindBy(id = "least-words")
    private WebElement leastWords;

    @FindBy(id = "without-words")
    private WebElement withoutWords;

    @FindBy(id = "title-is")
    private WebElement titleIs;

    @FindBy(id = "author-is")
    private WebElement authorIs;

    @FindBy(id = "date-facet-mode")
    private WebElement dateFacetMode;

    @FindBy(id = "facet-start-year")
    private WebElement facetStartYear;

    @FindBy(id = "facet-end-year")
    private WebElement facetEndYear;

    @FindBy(id = "results-only-access-checkbox-advanced")
    private WebElement resultsOnlyAccessCheckboxAdvanced;

    @FindBy(id = "submit-advanced-search")
    private WebElement submitAdvancedSearch;

    public SearchPage(WebDriver driver) {
        super(driver);
    }

    public void waitFor() {
        waitFor("submit-advanced-search");
    }

    public static SearchPage goTo(String baseUrl, WebDriver driver) {
        driver.get(baseUrl);
        SearchPage searchPage = new SearchPage(driver);
        searchPage.waitFor();
        return searchPage;
    }

    public static SearchPage goTo(WebDriver driver) {
        SearchPage searchPage = new SearchPage(driver);
        searchPage.waitFor();
        return searchPage;
    }

    public void setWords(String words) {
        this.words.sendKeys(words);
    }

    public void setExactPhrase(String exactPhrase) {
        this.exactPhrase.sendKeys(exactPhrase);
    }

    public void setLeastWords(String leastWords) {
        this.leastWords.sendKeys(leastWords);
    }

    public void setWithoutWords(String withoutWords) {
        this.withoutWords.sendKeys(withoutWords);
    }

    public void setTitleIs(String titleIs) {
        this.titleIs.sendKeys(titleIs);
    }

    public void setAuthorIs(String authorIs) {
        this.authorIs.sendKeys(authorIs);
    }

    public void setDateFacetMode(String dateFacetMode) {
        Select selectDateFacetMode = new Select(this.dateFacetMode);
        selectDateFacetMode.selectByVisibleText(dateFacetMode);
    }

    public void setFacetStartYear(String facetStartYear) {
        this.facetStartYear.sendKeys(facetStartYear);
    }

    public void setFacetEndYear(String facetEndYear) {
        this.facetEndYear.sendKeys(facetEndYear);
    }

    public void clickResultsOnlyAccessCheckboxAdvanced() {
        this.resultsOnlyAccessCheckboxAdvanced.click();
    }

    public SearchResult clickSubmitAdvancedSearch() {//
        this.submitAdvancedSearch.click();
        return SearchResult.goTo(driver);
    }
}
