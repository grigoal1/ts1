package cz.cvut.fel.ts1.selenium.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.stream.Collectors;

public class SearchResult extends Page{
    @FindBy(css = "#kb-nav--main > div.functions-bar.functions-bar-bottom > form > a")
    private WebElement nextBtn;

    @FindBy(css = "#kb-nav--main > div.functions-bar.functions-bar-bottom > form > span.prev")
    private WebElement prevBtn;

    @FindBy(css = "#content-type-facet > ol > li:nth-child(1) > a")
    private WebElement contentTypeArticle;//todo

    @FindBy(css = ".content-item-list .no-access")
    private List<WebElement> resultList;

    public SearchResult(WebDriver driver) {
        super(driver);
    }

    public void waitFor() {
        waitFor("results-list");
    }

    public static SearchResult goTo(String baseUrl, WebDriver driver) {
        driver.get(baseUrl);
        SearchResult searchResult = new SearchResult(driver);
        searchResult.waitFor();
        return searchResult;
    }

    public static SearchResult goTo(WebDriver driver) {
        SearchResult searchResult = new SearchResult(driver);
        searchResult.waitFor();
        return searchResult;
    }

    public SearchResult nextPage(){
        nextBtn.click();
        return new SearchResult(driver);//todo
    }

    public void clickContentTypeArticle(){
        contentTypeArticle.click();
    }

    public void clickElement(int i){
        resultList.get(i).findElement(By.className("title")).click();
    }
}
