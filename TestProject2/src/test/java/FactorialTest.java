import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FactorialTest {

    @Test
    public void factorialTest1(){
        assertEquals(24, MainClass.factorial(4));
    }

    @Test
    public void factorialTest2(){
        assertEquals(-1, MainClass.factorial(-3));
    }

    @Test
    public void factorialTest3(){
        assertEquals(1, MainClass.factorial(0));
    }

    @Test
    public void factorialTest4(){
        assertEquals(120, MainClass.factorial(5));
    }

    @Test
    public void factorialTest5(){
        assertEquals(720, MainClass.factorial(6));
    }

    @Test
    public void factorialTest7(){
        assertEquals(-1, MainClass.factorial(-10));
    }

}
