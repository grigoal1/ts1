package cz.cvut.fel.ts1;

public class Foo {

    public int function(){
        return 5;
    }

    public String function2(int i){
        switch (i){
            case 1: return "One";
            case 2: return "Two";
            default: throw new RuntimeException("Error");
        }
    }
}
