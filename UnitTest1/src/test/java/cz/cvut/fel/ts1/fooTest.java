package cz.cvut.fel.ts1;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class fooTest {
    /*
    static Foo foo;

    @BeforeAll
    public static void setUp(){
        foo = new Foo();
    }
     */
    Foo foo;

    @BeforeEach//kdyz mame stav
    public void setUp(){
        foo = new Foo();
    }

    @Test
    public void function_noInput_return5(){
//        Foo foo = new Foo();

        int result = foo.function();

        assertEquals(5, result);
    }

    @Test
    public void function2_inputOne_returnStringOne(){
//        Foo foo = new Foo();
        int intput = 1;
        String expectedResult = "One";

        String result = foo.function2(intput);

        assertEquals(expectedResult, result);
    }

    @Test
    public void function2_inputOne_returnStringTwo(){
//        Foo foo = new Foo();
        int intput = 2;
        String expectedResult = "Two";

        String result = foo.function2(intput);

        assertEquals(expectedResult, result);
    }
}