package cz.cvut.fel.ts1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    //ARRANGE
    Calculator c;
    int firstParameter;
    int secondParameter;

    @BeforeEach
    void setUp() {
        c = new Calculator();
    }

    @Test
    void add_inputOneAndZero_outputOne() {
        //arrange
        firstParameter = 1;
        secondParameter = 0;
        int expectedResult = 1;

        //ACT
        int result = c.add(firstParameter, secondParameter);

        //ASSERT
        assertEquals(expectedResult, result);
    }

    @Test
    void subtract_inputTwoAndMinusThree_outputFive() {
        firstParameter = 2;
        secondParameter = -3;
        int expectedResult = 5;

        //ACT
        int result = c.subtract(firstParameter, secondParameter);

        //ASSERT
        assertEquals(expectedResult, result);
    }

    @Test
    void multiply_inputTwoTimesZero_outputZero() {
        firstParameter = 2;
        secondParameter = 0;
        int expectedResult = 0;

        //ACT
        int result = c.multiply(firstParameter, secondParameter);

        //ASSERT
        assertEquals(expectedResult, result);
    }

    @Test
    void divide_inputFourDividedIntoTwo_outputTwoAndAHalf() {
        firstParameter = 4;
        secondParameter = 2;
        int expectedResult = 2;

        //ACT
        int result = c.divide(firstParameter, secondParameter);

        //ASSERT
        assertEquals(expectedResult, result);
    }

    @Test
    void divide_inputFiveDividedIntoZero_arithmeticException() {
        firstParameter = 5;
        secondParameter = 0;

        //ACT and ASSERT
        assertThrows(ArithmeticException.class, () -> c.divide(firstParameter, secondParameter));
    }


    //Next
    @ParameterizedTest(name ="name = {0}, {1}, {2}")
    @CsvSource({"1, 2, 3", "2, 3, 5", "3, 4, 7"})
    void add_addsAandB_returnsC(int newFirstParameter, int newSecondParameter, int newExpectedResult){

        //ACT
        int result = c.add(newFirstParameter, newSecondParameter);

        //ASSERT
        assertEquals(newExpectedResult, result);

    }

    @ParameterizedTest
    @MethodSource("getNumbers")
    public void divide_divideVy0_throwsExeption(int a){
        //ACT and ASSERT
        assertThrows(RuntimeException.class, () -> c.divide(a, 0));
    }

    public static int[] getNumbers(){
        return new int[]{1, 2, 3};
    }
}