package cz.cvut.fel.ts1.shop;

import cz.cvut.fel.ts1.storage.NoItemInStorage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class EShopControllerTest {

    //arrange
    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @BeforeEach
    void setUp() {
        EShopController.newInitStartEShop();
        newOutputForTest();
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    void EShopControllerFullCycle(){
        //arrange
        String parameter1 = "STORAGE IS CURRENTLY CONTAINING:\n" +
                "STOCK OF ITEM:  Item   ID 1   NAME Dancing Panda v.2   CATEGORY GADGETS   PRICE 5000.0  " +
                " LOYALTY POINTS 5    PIECES IN STORAGE: 20\n";
        String parameter2 = "Item with ID 1 added to the shopping cart.\n";
        String parameter3 = "ITEM PURCHASE STATISTICS:\n" +
                "ITEM  Item   ID 1   NAME Dancing Panda v.2   CATEGORY GADGETS   PRICE 5000.0" +
                "   LOYALTY POINTS 5   HAS BEEN SOLD 1 TIMES\n";
        String parameter4 = "STORAGE IS CURRENTLY CONTAINING:\n" +
                "STOCK OF ITEM:  Item   ID 1   NAME Dancing Panda v.2   CATEGORY GADGETS   PRICE 5000.0" +
                "   LOYALTY POINTS 5    PIECES IN STORAGE: 19\n";
        Item newItem = new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5);

        //act
        EShopController.getStorage().insertItems(newItem, 20);
        EShopController.getStorage().printListOfStoredItems();

        //assert
        assertEquals(parameter1, outContent.toString());
        newOutputForTest();

        //act
        ShoppingCart newCart = new ShoppingCart();
        newCart.addItem(newItem);

        //assert
        assertEquals(parameter2, outContent.toString());
        newOutputForTest();

        //act
        try {
            EShopController.purchaseShoppingCart(newCart, "Libuse Novakova","Kosmonautu 25, Praha 8");
        } catch (NoItemInStorage noItemInStorage) {
            noItemInStorage.printStackTrace();
            System.err.println("There should be no thrown");
        }
        EShopController.getArchive().printItemPurchaseStatistics();

        //assert
        assertEquals(parameter3, outContent.toString());
        newOutputForTest();

        //act
        EShopController.getStorage().printListOfStoredItems();

        //assert
        assertEquals(parameter4, outContent.toString());
        newOutputForTest();
    }

    @Test
    void EShopControllerErrorNoItemInStorage(){
        //arrange
        String parameter1 = "STORAGE IS CURRENTLY CONTAINING:\n" +
                "STOCK OF ITEM:  Item   ID 1   NAME Dancing Panda v.2   CATEGORY GADGETS   PRICE 5000.0  " +
                " LOYALTY POINTS 5    PIECES IN STORAGE: 20\n";
        Item newItem1 = new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5);
        Item newItem2 = new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10);

        //act
        EShopController.getStorage().insertItems(newItem1, 20);
        EShopController.getStorage().printListOfStoredItems();

        //assert
        assertEquals(parameter1, outContent.toString());
        newOutputForTest();

        //act
        ShoppingCart newCart = new ShoppingCart();
        newCart.addItem(newItem2);

        //assert
        assertThrows(NoItemInStorage.class, () -> EShopController.purchaseShoppingCart(newCart, "Libuse Novakova","Kosmonautu 25, Praha 8"));
        //but must be thrown
    }

    @Test
    void EShopControllerErrorNoMoreProduct() throws NoItemInStorage {
        //arrange
        String parameter1 = "STORAGE IS CURRENTLY CONTAINING:\n" +
                "STOCK OF ITEM:  Item   ID 1   NAME Dancing Panda v.2   CATEGORY GADGETS   PRICE 5000.0  " +
                " LOYALTY POINTS 5    PIECES IN STORAGE: 1\n";
        String parameter2 = "Error: shopping cart is empty\n";
        String parameter3 = "Item with ID 1 added to the shopping cart.\n";
        String parameter4 = "ITEM PURCHASE STATISTICS:\n" +
                "ITEM  Item   ID 1   NAME Dancing Panda v.2   CATEGORY GADGETS   PRICE 5000.0" +
                "   LOYALTY POINTS 5   HAS BEEN SOLD 1 TIMES\n";
        String parameter5 = "STORAGE IS CURRENTLY CONTAINING:\n" +
                "STOCK OF ITEM:  Item   ID 1   NAME Dancing Panda v.2   CATEGORY GADGETS   PRICE 5000.0" +
                "   LOYALTY POINTS 5    PIECES IN STORAGE: 0\n";
        Item newItem = new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5);

        //act
        EShopController.getStorage().insertItems(newItem, 1);
        EShopController.getStorage().printListOfStoredItems();

        //assert
        assertEquals(parameter1, outContent.toString());
        newOutputForTest();

        //act
        ShoppingCart newCart = new ShoppingCart();
        EShopController.purchaseShoppingCart(newCart, "Libuse Novakova","Kosmonautu 25, Praha 8");

        //assert
        assertEquals(parameter2, outContent.toString());
        newOutputForTest();

        //act
        newCart.addItem(newItem);

        //assert
        assertEquals(parameter3, outContent.toString());
        newOutputForTest();

        //assert
        newOutputForTest();

        //act
        EShopController.purchaseShoppingCart(newCart, "Libuse Novakova","Kosmonautu 25, Praha 8");
        EShopController.getArchive().printItemPurchaseStatistics();

        //assert
        assertEquals(parameter4, outContent.toString());
        newOutputForTest();
        //act
        EShopController.getStorage().printListOfStoredItems();

        //assert
        assertEquals(parameter5, outContent.toString());
        newOutputForTest();
    }
    @Test
    void EShopControllerEmptyCart(){
        //arrange
        String parameter1 = "STORAGE IS CURRENTLY CONTAINING:\n" +
                "STOCK OF ITEM:  Item   ID 1   NAME Dancing Panda v.2   CATEGORY GADGETS   PRICE 5000.0  " +
                " LOYALTY POINTS 5    PIECES IN STORAGE: 1\n";
        String parameter2 = "Item with ID 1 added to the shopping cart.\n" +
                "Item with ID 1 added to the shopping cart.\n";
        Item newItem1 = new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5);

        //act
        EShopController.getStorage().insertItems(newItem1, 1);
        EShopController.getStorage().printListOfStoredItems();

        //assert
        assertEquals(parameter1, outContent.toString());
        newOutputForTest();

        //act
        ShoppingCart newCart = new ShoppingCart();
        newCart.addItem(newItem1);
        newCart.addItem(newItem1);
        assertEquals(parameter2, outContent.toString());
        newOutputForTest();

        //assert
        assertThrows(NoItemInStorage.class, () -> EShopController.purchaseShoppingCart(newCart, "Libuse Novakova","Kosmonautu 25, Praha 8"));
        //but must be thrown
    }



    private void newOutputForTest(){
        outContent = new ByteArrayOutputStream();
        errContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }
}