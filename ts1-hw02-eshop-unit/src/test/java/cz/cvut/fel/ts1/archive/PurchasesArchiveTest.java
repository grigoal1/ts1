package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.Order;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.mail.Message;
import javax.mail.Transport;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.whenNew;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ Transport.class })
public class PurchasesArchiveTest {

    @Test
    public void printItemPurchaseStatisticsTest() {
        // arrange
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        final PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outContent));
        String output = "ITEM PURCHASE STATISTICS:\n" +
                "print: Mock for ItemPurchaseArchiveEntry(method toString())\n";

        ArrayList<Order> orderArchive = mock(ArrayList.class);
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = mock(ItemPurchaseArchiveEntry.class);
        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap();
        itemArchive.put(1, itemPurchaseArchiveEntry);
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchive, orderArchive);
        when(itemPurchaseArchiveEntry.toString()).thenReturn("print: Mock for ItemPurchaseArchiveEntry(method toString())");

        //act
        purchasesArchive.printItemPurchaseStatistics();

        //assert
        assertEquals(output, outContent.toString());
        System.setOut(originalOut);
    }

    @Test
    public void secondTest() {
        // arrange
        ArrayList<Order> orderArchive = mock(ArrayList.class);
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = mock(ItemPurchaseArchiveEntry.class);
        Item item = mock(Item.class);
        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap();
        itemArchive.put(1, itemPurchaseArchiveEntry);
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchive, orderArchive);
        when(itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold()).thenReturn(1);
        when(item.getID()).thenReturn(1);

        //act and assert
        assertEquals(1, purchasesArchive.getHowManyTimesHasBeenItemSold(item));

        when(item.getID()).thenReturn(2);

        //act and assert
        assertEquals(0, purchasesArchive.getHowManyTimesHasBeenItemSold(item));
    }

    @Test
    public void thirdTest() throws Exception {
        // arrange
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        final PrintStream originalOut = System.out;
        System.setOut(new PrintStream(outContent));
        String output = "ITEM PURCHASE STATISTICS:  \n";

        Order order = mock(Order.class);
        Item item = mock(Item.class);
        ArrayList<Item> items = new ArrayList<Item>();
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = mock(ItemPurchaseArchiveEntry.class);
        PurchasesArchive purchasesArchive = new PurchasesArchive();
        items.add(item);

        //action
        when(order.getItems()).thenReturn(items);
        whenNew(ItemPurchaseArchiveEntry.class).withAnyArguments().thenReturn(itemPurchaseArchiveEntry);
        purchasesArchive.putOrderToPurchasesArchive(order);
        purchasesArchive.printItemPurchaseStatistics();



        //assert
        assertTrue(output.length() <  outContent.toString().length());

        System.setOut(originalOut);
    }


}