package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import org.junit.jupiter.api.*;

class OrderTest {
    //arrange
    ShoppingCart newCart;
    String customerName = "customer name";
    String customerAddress = "customer address";

    @Test
    void firstTest(){
        //action
        startInit();
        Order order = new Order(newCart, "customer name", "customer address");

        //assert
        assertEquals(order.customerAddress, customerAddress);
        assertEquals(order.customerName, customerName);
        assertEquals(order.state, 0);
        assertEquals(order.getItems(), newCart.getCartItems());
    }
    @Test
    void secondTest(){
        //action
        startInit();
        Order order = new Order(newCart, "customer name", "customer address", 2);

        //assert
        assertEquals(order.customerAddress, customerAddress);
        assertEquals(order.customerName, customerName);
        assertEquals(order.state, 2);
        assertEquals(order.getItems(), newCart.getCartItems());
    }




    private void startInit() {

        Item[] storageItems = {
                new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
                new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
                new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
                new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
                new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
                new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
        };

        newCart = new ShoppingCart();
        newCart.addItem(storageItems[0]);
        newCart.addItem(storageItems[1]);
        newCart.addItem(storageItems[2]);
        newCart.addItem(storageItems[4]);
        newCart.addItem(storageItems[5]);
    }


}