package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;



class StandardItemTest {


    @Test
    public void constructor_negativePrices_exeption(){

        //assert
        assertThrows(Exception.class, () -> new StandardItem(1, "book1", -1, "Books", 2));

    }

    @ParameterizedTest
    @CsvSource({"1, name, 200, book, 2", "2, , 200, book, 3", "3, name, 200, , 3"})
    void equals_identicalObjectsWithNull_true(int id, String name, float price, String category, int loyaltyPoints) {

        //arrange and action
        StandardItem si1 = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem si2 = new StandardItem(id, name, price, category, loyaltyPoints);
        assertDoesNotThrow(() -> si1.equals(si2));
        assertTrue(si1.equals(si2));
    }

    @ParameterizedTest
    @CsvSource({"1, name, 200, book, 3, 2, , 200, book, 3", "1, name, 200, book, 3, 2, , 200, book, 3",
            "1, , 200, book, 3, 2, name, 200, book, 3", "1, name, 200, book, 3, 2, name, 200, , 3",
            "1, name, 200, book, 3, 1, name, 200, book2, 3","1, name2, 200, book, 3, 1, name, 200, book, 3",
            "2, name, 200, book, 3, 1, name, 200, book, 3","1, name, 200, book, 4, 1, name, 200, book, 3"})
    void equals_differentObjectsWithNull_false(int id1, String name1, float price1, String category1, int loyaltyPoints1,
                                      int id2, String name2, float price2, String category2, int loyaltyPoints2) {

        //arrange and action
        StandardItem si1 = new StandardItem(id1, name1, price1, category1, loyaltyPoints1);
        StandardItem si2 = new StandardItem(id2, name2, price2, category2, loyaltyPoints2);
        assertDoesNotThrow(() -> si1.equals(si2));
        assertFalse(si1.equals(si2));
    }

    @ParameterizedTest
    @CsvSource({"1, name, 200, book, 2", "1, , 200, book, 2", "1, name, 200, , 2", "1, name, 200, book, 2"})
    void equals_copyObjectsWithNull_true(int id, String name, float price, String category, int loyaltyPoints) {

        //arrange and action
        StandardItem si1 = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem si2 = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem si3 = si1.copy();

        //assert
        assertTrue(si1.equals(si3));

        //action
        si3.setLoyaltyPoints(loyaltyPoints + 1);
        si3.setCategory("new " + category);
        si3.setID(id + 1);
        si3.setName("new " + name);
        si3.setPrice(price + 1);

        //assert
        assertTrue(si1.equals(si2));
    }
}