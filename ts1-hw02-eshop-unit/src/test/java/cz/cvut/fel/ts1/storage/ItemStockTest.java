package cz.cvut.fel.ts1.storage;


import cz.cvut.fel.ts1.shop.DiscountedItem;
import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.ShoppingCart;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class ItemStockTest {
    Item[] storageItems;

    @ParameterizedTest(name = "increase = {1}, decrease = {2}")
    @CsvSource({"0, 10, 20", "1, 20, 10", "2, 0, 0", "3, 10, 0", "4, -10, 10", "5, 10, -10"})
    void ItemStockTest(int idx, int increase, int decrease){
        startInit();
        ItemStock is = new ItemStock(storageItems[idx]);

        assertEquals(is.getCount(),0);
        if(increase < 0 && decrease < 0){
            assertThrows(Exception.class, () -> is.IncreaseItemCount(increase));
            assertThrows(Exception.class, () -> is.decreaseItemCount(decrease));

        }else if (increase < 0){
            assertThrows(Exception.class, () -> is.IncreaseItemCount(increase));
        }else if (decrease < 0){
            assertThrows(Exception.class, () -> is.decreaseItemCount(decrease));
        }else if (increase - decrease < 0){
            is.IncreaseItemCount(increase);
            assertThrows(Exception.class, () -> is.decreaseItemCount(decrease));
        }else {
            is.IncreaseItemCount(increase);
            assertEquals(is.getCount(), increase);
            is.decreaseItemCount(decrease);
            assertEquals(is.getCount(), increase - decrease);
        }

    }




    private void startInit() {
        storageItems = new Item[]{
                new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
                new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
                new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
                new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
                new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
                new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
        };
    }
}